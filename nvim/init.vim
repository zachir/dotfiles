" vimplugs {{{
call plug#begin('~/.config/nvim/plugged')
" colorschemes {{{
Plug 'tomasiser/vim-code-dark'        " codedark color scheme
Plug 'flazz/vim-colorschemes'         " meta-color scheme package
" }}}
" language support {{{
Plug 'axvr/org.vim'                   " basic org markup language implementation
Plug 'ziglang/zig.vim'                " zig programming language
Plug 'jdonaldson/vaxe'                " haxe programming language
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }  " latex live preview support
Plug 'waycrate/swhkd-vim'             " swhkd syntax highlighting
Plug 'HiPhish/guile.vim'              " guile syntax highlighting
" }}}
" utility {{{
Plug 'preservim/nerdtree'             " directory tree interface
Plug 'Xuyuanp/nerdtree-git-plugin'    " git status indicator for nerdtree
Plug 'PhilRunninger/nerdtree-visual-selection' " allows visual selection mode in nerdtree
Plug 'tpope/vim-fugitive'             " git command interface
Plug 'junegunn/goyo.vim'              " simplify the user interface (distraction-free)  [BROKEN]
Plug 'junegunn/limelight.vim'         " focus on single paragraph of text at a time
Plug 'vim-airline/vim-airline'        " powerline the vim bottom bar
Plug 'nathanaelkane/vim-indent-guides' " indent guides for vim
Plug 'mhinz/vim-signify'              " use signs to show diffs
Plug 'tpope/vim-endwise'              " auto end functions/if statements
Plug 'tpope/vim-surround'             " auto close stuff
Plug 'mattn/emmet-vim'                " html css easy formating
Plug 'godlygeek/tabular'              " Needed for vim-markdown
Plug 'plasticboy/vim-markdown'        " Better markdown support
Plug 'ryanoasis/vim-devicons' " dev icons for nerdtree
" }}}
call plug#end()

" }}}
" nvim options {{{

"" nvimrc:
"" General

set showmatch	" Highlight matching brace

set number
set relativenumber

colorscheme BlackSea
 
set hlsearch	" Highlight all search results
set smartcase	" Enable smart-case search
set incsearch	" Searches for strings incrementally
 
set expandtab	" Use spaces instead of tabs
set shiftwidth=2	" Number of auto-indent spaces
set smartindent	" Enable smart-indent
set smarttab	" Enable smart-tabs
set softtabstop=2	" Number of spaces per Tab	

"" Advanced
set ruler	" Show row and column ruler information
set undolevels=1000	" Number of undo levels
set backspace=indent,eol,start	" Backspace behaviour
filetype plugin indent on
 
"" GUI Config
set guifont=mononoki\ Nerd\ Font\ Mono:h12

hi Normal guibg=NONE ctermbg=NONE
hi NonText guibg=NONE ctermbg=NONE
set background=dark

set foldmethod=marker

set conceallevel=2

" set leader key

let mapleader="'"

" emmet {{{
let g:user_emmet_leader_key='<C-a>'
" }}}

" zig config {{{
let g:zig_fmt_autosave = 1
" }}}

" latex live preview {{{
let g:livepreview_previewer = 'zathura'
let g:livepreview_use_biber = 1
let g:livepreview_cursorhold_recompile = 0
" }}}

" vim-markdown {{{
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_folding_level = 3
let g:vim_markdown_toc_autofit = 1
let g:vim_markdown_emphasis_multiline = 0
let g:tex_conceal = ""
let g:vim_markdown_math = 1
let g:vim_markdown_conceal_code_blocks = 1
let g:vim_markdown_fenced_languages = ['c++=cpp', 'viml=vim', 'bash=sh', 'ini=dosini']
let g:vim_markdown_follow_anchor = 1
let g:vim_markdown_math = 1
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_toml_frontmatter = 1
let g:vim_markdown_strikethrough = 1
let g:vim_markdown_no_extensions_in_markdown = 1
let g:vim_markdown_autowrite = 1
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_new_list_item_indent = 0
let g:vim_markdown_edit_url_in = 'tab'
" }}}

" NERDTree config {{{
let g:NERDTreeGitStatusUseNerdFonts = 1 " use nerd fonts
let g:NERDTreeGitStatusShowClean = 1 " default: 0
let g:NERDTreeDirArrowExpandable = '>'
let g:NERDTreeDirArrowCollapsible = '<'
let g:NERDTreeMapToggleHidden = 'z'
" }}}

" Goyo config {{{
let g:goyo_width = '90%'
let g:goyo_height = '100%'
" }}}

" limelight config {{{
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_guifg = 'gray'
" }}}

" devicon config {{{
let g:airline_powerline_fonts = 1
let g:webdevicons_enable_nerdtree = 1
" }}}

" vim-indent-guides config {{{
let g:indent_guides_enable_on_vim_startup = 0
" }}}

" vim-signify config {{{
set updatetime=100
" }}}

" }}}
" Functions {{{
" Goyo functions {{{
function! s:goyo_enter()
  if executable('tmux') && strlen($TMUX)
    silent !tmux set status off
    silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
  endif
  set noshowmode
  set noshowcmd
  set scrolloff=999
  Limelight
  NERDTreeClose
endfunction
function! s:goyo_leave()
  if executable('tmux') && strlen('$TMUX')
    silent !tmux set status on
    silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
  endif
  set showmode
  set showcmd
  set scrolloff=5
  Limelight!
  NERDTree
endfunction
" }}}
" }}}
" Autocommands {{{
" Goyo autocommands {{{
autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()
" }}}
" NERDTree autocmds {{{
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | wincmd p | endif
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') |
      \ execute 'NERDTree' argv()[0] | wincmd p | enew | execute 'cd '.argv()[0] | endif
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
"autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
      "\ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer;.buf | endif
autocmd BufWinEnter * if getcmdwintype() == '' | silent NERDTreeMirror | endif
" }}}
" custom augroup {{{
augroup custom
  autocmd!
  au BufEnter     *.tex :LLPStartPreview
  au BufEnter     *.md :set textwidth=80
  au BufEnter     *.ms :set textwidth=80
  au BufWritePost init.vim :source %
  au BufWritePost *.tex :make
  au BufWritePost *sxhkdrc* :!pkill -USR1 -x sxhkd
  au BufWritePost .Xresources :!xrdb ~/.Xresources
  au BufWritePost .gitignore :!git add .gitignore
augroup end
" }}}
" }}}
" keybindings {{{

inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk
inoremap <Home> <C-o>g<Home>
inoremap <End> <C-o>g<End>
vnoremap <Down> <C-o>gj
vnoremap <Up> <C-o>gk
vnoremap <Home> <C-o>g<Home>
vnoremap <End> <C-o>g<End>
noremap <Down> gj
noremap <Up> gk

" toggles
noremap <leader>tn :set number!<CR>
noremap <leader>tr :set relativenumber!<CR>
noremap <leader>tt :NERDTreeToggle<CR>

" tabs
noremap <leader>th :tabprevious<CR>
noremap <leader>tl :tabnext<CR>
noremap <leader>tk :tabnew<CR>
noremap <leader>tj :tabclose<CR>

" splits
noremap <leader>wh :vertical resize -1<CR>
noremap <leader>wj :resize -1<CR>
noremap <leader>wk :resize +1<CR>
noremap <leader>wl :vertical resize +1<CR>

" config
noremap <leader>en :e ~/.config/nvim/init.vim<CR>
noremap <leader>ec :source %<CR>

" write as root
noremap ZW :w !pkexec tee % >/dev/null

" Goyo
noremap <leader>G :Goyo<CR>

" windows
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
vnoremap <C-h> <C-w>h
vnoremap <C-j> <C-w>j
vnoremap <C-k> <C-w>k
vnoremap <C-l> <C-w>l

" files
" these may go away soon
noremap <leader>xx :q<CR>
noremap <leader>x! :q!<CR>
" these are the new standard
noremap <leader>qq :q<CR>
noremap <leader>q! :q!<CR>
noremap <leader>wq :wq<CR>
noremap <leader>ww :w<CR>
noremap <leader>ee :e 

" escape for term windows
tnoremap <Esc> <C-\><C-n>

" vim-plug
noremap <leader>pi :PlugInstall<CR>
noremap <leader>pu :PlugUpdate<CR>
noremap <leader>pU :PlugUpgrade<CR>
noremap <leader>pc :PlugClean<CR>

" fugitive
noremap <leader>gp :G pull<CR>
noremap <leader>gd :G diff %<CR>

" }}}
