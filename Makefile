VIM := $(shell command -v vim 2>/dev/null)
NVIM := $(shell command -v nvim 2>/dev/null)
XCONFS := X11/Xresources
ZCONFS := zsh/.zshenv
DCONFS := doas.conf

all:

install: install-xconfigs install-zshconfigs vimplug-vim vimplug-nvim doas-conf

install-xconfigs: $(XCONFS)
	@echo "Installing Xorg conf files..."
	@echo "Xresources..."
	@ln -sf `pwd`/X11/Xresources ~/.Xresources
	@echo "Done."

install-zshconfigs: $(ZCONFS)
	@echo "Installing zsh conf files..."
	@echo ".zshenv..."
	@ln -sf `pwd`/zsh/.zshenv ~/.zshenv
	@echo "Done."

vimplug-vim: installers/vimplug_vim.sh
ifdef VIM
	@echo "Installing vim-plug for vim..."
	@$(shell installers/vimplug_vim.sh)
	@echo "Done."
else
	@echo "vim not installed."
endif

vimplug-nvim: installers/vimplug_nvim.sh
ifdef NVIM
	@echo "Installing vim-plug for nvim..."
	@$(shell installers/vimplug_nvim.sh)
	@echo "Done."
else
	@echo "nvim not installed."
endif

doas-conf: $(DCONFS)
	@echo "Installing doas config files..."
	@echo "doas.conf..."
	@sudo cp -n doas.conf /etc/doas.conf
	@echo "Done."
