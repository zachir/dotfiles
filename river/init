#!/bin/sh

# This is the example configuration file for river.
#
# If you wish to edit this, you will probably want to copy it to
# $XDG_CONFIG_HOME/river/init or $HOME/.config/river/init first.
#
# See the river(1), riverctl(1), and rivertile(1) man pages for complete
# documentation.

# Note: the "$mod1" modifier is also known as Logo, GUI, Windows, Mod4, etc.

mod1="Alt"
mod2="Super"

# $mod1+Shift+Return to start an instance of foot (https://codeberg.org/dnkl/foot)
riverctl map normal $mod1 Return spawn foot

# $mod1+Q to close the focused view
riverctl map normal $mod1+Shift Q close

# $mod1+Shift+E to exit river
riverctl map normal $mod1+Shift E exit

# $mod1+J and $mod1+K to focus the next/previous view in the layout stack
riverctl map normal $mod1 J focus-view next
riverctl map normal $mod1 K focus-view previous

# $mod1+Shift+J and $mod1+Shift+K to swap the focused view with the next/previous
# view in the layout stack
riverctl map normal $mod1+Shift J swap next
riverctl map normal $mod1+Shift K swap previous

# $mod1+Period and $mod1+Comma to focus the next/previous output
riverctl map normal $mod1 Period focus-output next
riverctl map normal $mod1 Comma focus-output previous

# $mod1+Shift+{Period,Comma} to send the focused view to the next/previous output
riverctl map normal $mod1+Shift Period send-to-output next
riverctl map normal $mod1+Shift Comma send-to-output previous

# $mod1+Return to bump the focused view to the top of the layout stack
riverctl map normal $mod1+Shift Return zoom

# $mod1+H and $mod1+L to decrease/increase the main ratio of rivertile(1)
riverctl map normal $mod1 H send-layout-cmd rivertile "main-ratio -0.05"
riverctl map normal $mod1 L send-layout-cmd rivertile "main-ratio +0.05"

# $mod1+Shift+H and $mod1+Shift+L to increment/decrement the main count of rivertile(1)
riverctl map normal $mod1+Shift H send-layout-cmd rivertile "main-count +1"
riverctl map normal $mod1+Shift L send-layout-cmd rivertile "main-count -1"

# $mod1+$mod2+{H,J,K,L} to move views
riverctl map normal $mod1+$mod2 H move left 100
riverctl map normal $mod1+$mod2 J move down 100
riverctl map normal $mod1+$mod2 K move up 100
riverctl map normal $mod1+$mod2 L move right 100

# $mod1+$mod2+Control+{H,J,K,L} to snap views to screen edges
riverctl map normal $mod1+$mod2+Control H snap left
riverctl map normal $mod1+$mod2+Control J snap down
riverctl map normal $mod1+$mod2+Control K snap up
riverctl map normal $mod1+$mod2+Control L snap right

# $mod1+$mod2+Shift+{H,J,K,L} to resize views
riverctl map normal $mod1+$mod2+Shift H resize horizontal -100
riverctl map normal $mod1+$mod2+Shift J resize vertical 100
riverctl map normal $mod1+$mod2+Shift K resize vertical -100
riverctl map normal $mod1+$mod2+Shift L resize horizontal 100

# $mod1 + Left Mouse Button to move views
riverctl map-pointer normal $mod1 BTN_LEFT move-view

# $mod1 + Right Mouse Button to resize views
riverctl map-pointer normal $mod1 BTN_RIGHT resize-view

# $mod1 + Middle Mouse Button to toggle float
riverctl map-pointer normal $mod1 BTN_MIDDLE toggle-float

# Mouse rules
riverctl set-cursor-warp on-output-change
riverctl focus-follors-cursor always

for i in $(seq 1 9)
do
    tags=$((1 << ($i - 1)))

    # $mod1+[1-9] to focus tag [0-8]
    riverctl map normal $mod1 $i set-focused-tags $tags

    # $mod1+Shift+[1-9] to tag focused view with tag [0-8]
    riverctl map normal $mod1+Shift $i set-view-tags $tags

    # $mod1+Ctrl+[1-9] to toggle focus of tag [0-8]
    riverctl map normal $mod1+Control $i toggle-focused-tags $tags

    # $mod1+Shift+Ctrl+[1-9] to toggle tag [0-8] of focused view
    riverctl map normal $mod1+Shift+Control $i toggle-view-tags $tags
done

# $mod1+0 to focus all tags
# $mod1+Shift+0 to tag focused view with all tags
all_tags=$(((1 << 9) - 1))
riverctl map normal $mod1 0 set-focused-tags $all_tags
riverctl map normal $mod1+Shift 0 set-view-tags $all_tags

# scratchpad tags
BINPATH="${XDG_CONFIG_HOME:-$HOME/.config}/river/sp"
riverctl map normal $mod1+Control z spawn "$BINPATH/river_sp z"
riverctl map normal $mod1+Control x spawn "$BINPATH/river_sp x"
riverctl map normal $mod1+Control c spawn "$BINPATH/river_sp c"
riverctl map normal $mod1+Control v spawn "$BINPATH/river_sp v"
riverctl map normal $mod1+Control b spawn "$BINPATH/river_sp b"
riverctl map normal $mod1+Control a spawn "$BINPATH/river_sp a"
riverctl map normal $mod1+Control s spawn "$BINPATH/river_sp s"
riverctl map normal $mod1+Control d spawn "$BINPATH/river_sp d"
riverctl map normal $mod1+Control f spawn "$BINPATH/river_sp f"
riverctl map normal $mod1+Control g spawn "$BINPATH/river_sp g"
riverctl map normal $mod1+Control q spawn "$BINPATH/river_sp q"

# $mod1+Space to toggle float
riverctl map normal $mod1+Control Space toggle-float

# $mod1+F to toggle fullscreen
riverctl map normal $mod1 F toggle-fullscreen

# $mod1+{Up,Right,Down,Left} to change layout orientation
riverctl map normal $mod1 Up    send-layout-cmd rivertile "main-location top"
riverctl map normal $mod1 Right send-layout-cmd rivertile "main-location right"
riverctl map normal $mod1 Down  send-layout-cmd rivertile "main-location bottom"
riverctl map normal $mod1 Left  send-layout-cmd rivertile "main-location left"

# Declare a passthrough mode. This mode has only a single mapping to return to
# normal mode. This makes it useful for testing a nested wayland compositor
riverctl declare-mode passthrough

# $mod1+F11 to enter passthrough mode
riverctl map normal $mod1 b enter-mode passthrough

# $mod1+F11 to return to normal mode
riverctl map passthrough $mod1 b enter-mode normal

# Various media key mapping examples for both normal and locked mode which do
# not have a modifier
for mode in normal locked
do
    # Eject the optical drive (well if you still have one that is)
    riverctl map $mode None XF86Eject spawn 'eject -T'

    # Control pulse audio volume with pamixer (https://github.com/cdemoulins/pamixer)
    riverctl map $mode None XF86AudioRaiseVolume  spawn 'volsv -i'
    riverctl map $mode None XF86AudioLowerVolume  spawn 'volsv -d'
    riverctl map $mode None XF86AudioMute         spawn 'volsv -t'
    riverctl map $mode None XF86AudioMicMute      spawn 'volsv -m'

    # Control MPRIS aware media players with playerctl (https://github.com/altdesktop/playerctl)
    riverctl map $mode None XF86AudioMedia spawn 'playerctl play-pause'
    riverctl map $mode None XF86AudioPlay  spawn 'playerctl play-pause'
    riverctl map $mode None XF86AudioPrev  spawn 'playerctl previous'
    riverctl map $mode None XF86AudioNext  spawn 'playerctl next'

    # Control screen backlight brightness with light (https://github.com/haikarainen/light)
    riverctl map $mode None XF86MonBrightnessUp   spawn 'bl -i 5'
    riverctl map $mode None XF86MonBrightnessDown spawn 'bl -d 5'
done

# add browser shortcuts
riverctl map normal $mod1+$mod2 1 spawn "bm -w"
riverctl map normal $mod1+$mod2 q spawn "qbc -w"
riverctl map normal $mod1+$mod2 w spawn "lwc -w"
riverctl map normal $mod1+$mod2 e spawn "ffc -w"

# add other menu shortcuts
riverctl map normal $mod1+$mod2 p spawn passmenu
riverctl map normal $mod1 r spawn 'exec $(tofi-drun)'
riverctl map normal $mod1 d spawn 'exec $(tofi-run)'
riverctl map normal $mod1+$mod2+Control u spawn 'dmenuunicode -w'
riverctl map normal $mod1+$mod2 Comma spawn 'dmenumount -w'
riverctl map normal $mod1+$mod2 Period spawn 'dmenuumount -w'
riverctl map normal $mod1+$mod2 b spawn 'dmenu_books -w'
riverctl map normal $mod1+$mod2 u spawn 'mprisctl -w'

# add other shortcuts
riverctl map normal $mod1 q spawn 'loginctl lock-session self'
riverctl map normal $mod1+$mod2 f spawn 'foot lf'
riverctl map normal $mod1+$mod2 m spawn 'volsv -t'
riverctl map normal $mod1+$mod2+Shift m spawn 'volsv -m'

# secondary mod shortcuts
riverctl map normal $mod2 p spawn 'playerctl play-pause'

# Set background and border color
riverctl background-color 0x000000
riverctl border-color-focused 0x535d6c
riverctl border-color-unfocused 0x000000

# Set keyboard repeat rate
riverctl set-repeat 50 300

# Make certain views start floating
riverctl float-filter-add app-id float
riverctl float-filter-add app-id "sphtop"
riverctl float-filter-add app-id "spterm"
riverctl float-filter-add app-id "sppmxr"
riverctl float-filter-add app-id "spblue"
riverctl float-filter-add app-id "spncmp"
riverctl float-filter-add app-id "spmutt"
riverctl float-filter-add app-id "spprof"
riverctl float-filter-add app-id "spirss"
riverctl float-filter-add app-id "sptodo"
riverctl float-filter-add app-id "sptrmc"
riverctl float-filter-add app-id "qpwgraph"
riverctl float-filter-add title "popup title with spaces"

# Set app-ids and titles of views which should use client side decorations
riverctl csd-filter-add app-id "gedit"

# Set the default layout generator to be rivertile and start it.
# River will send the process group of the init executable SIGTERM on exit.
riverctl default-layout rivertile
rivertile -view-padding 6 -outer-padding 6 &

sh ~/.config/autostart.sh &
