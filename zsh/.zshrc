HISTFILE="$ZDOTDIR"/hist
SAVEHIST=1000
HISTSIZE=1000
setopt APPEND_HISTORY
setopt HIST_FIND_NO_DUPS
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_VERIFY
setopt HIST_IGNORE_DUPS
setopt INC_APPEND_HISTORY
setopt HIST_REDUCE_BLANKS

_SH="zsh"

if [ -f "$XDG_CONFIG_HOME"/sh/aliases ]; then
	source "$XDG_CONFIG_HOME"/sh/aliases
fi
if [ -f "$XDG_CONFIG_HOME"/sh/functions ]; then
	source "$XDG_CONFIG_HOME"/sh/functions
fi

if [ -n "$ZDOTDIR"/.zshrc -a -f "$ZDOTDIR"/.zshrc ]; then
  alias e='source "$ZDOTDIR"/.zshrc'
elif [ -f "$HOME"/.zshrc ]; then
  alias e='source ~/.zshrc'
fi

bindkey "^?" backward-delete-char

# bindkey -v

if [ "$TERM" = "st-256color" ] || [ "$TERM" = "xterm-256color" ]; then
  function zle-line-init () { echoti smkx }
  function zle-line-finish () { echoti rmkx }

  zle -N zle-line-init
  zle -N zle-line-finish
fi

unset COLOR1
ZSHTHEMERC="${ZDOTDIR:-$HOME}/.zsh_theme"
[ -f "$ZSHTHEMERC" ] && . "$ZSHTHEMERC"

if [ -z "$COLOR1" ]; then
  case "$(whoami)" in
    zachir) COLOR1="014" ;;
    git) COLOR1="013" ;;
    subsonic) COLOR1="012" ;;
    *) COLOR1="015" ;;
  esac
fi

MYPROMPT='%B%S%F{'"$COLOR1"'}[%n@%m]:%f%s%F{015}%~%f%S%F{'"$COLOR1"'}%#%f%s%b '
function zle-line-init zle-keymap-select {
	VIM_PROMPT="%B%F{012}[%n@%m]:%f%F{015}%~%f%F{012}%#%f%b "
	PS1="${${KEYMAP/vicmd/$VIM_PROMPT}/main/$MYPROMPT}"
	#PS1="$PROMPT $KEYMAP"
	zle reset-prompt
}

unset COLOR1

zle -N zle-line-init
zle -N zle-keymap-select
export KEYTIMEOUT=1

autoload -Uz compinit promptinit
compinit -d $CONFIG/zcompdump
promptinit
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

zstyle ':completion:*' menu select
zstyle ':completion::complete:*' gain-privileges 1
zstyle ':completion:*' rehash true

setopt COMPLETE_ALIASES
setopt AUTO_CD

typeset -g -A key

key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[Shift-Tab]="${terminfo[kcbt]}"
key[Delete]="${terminfo[kdch1]}"

[[ -n "${key[Up]}" ]] && bindkey -- "${key[Up]}"                up-line-or-beginning-search
bindkey -- '^[[A'                                               up-line-or-beginning-search
bindkey -- '^[[B'                                               down-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}"            down-line-or-beginning-search
[[ -n "${key[Left]}" ]] && bindkey -- "${key[Left]}"            backward-char
[[ -n "${key[Right]}" ]] && bindkey -- "${key[Right]}"          forward-char
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete
[[ -n "${key[Delete]}" ]] && bindkey -- "${key[Delete]}"        delete-char

#eval "$(starship init zsh)"
type portmaster-start 2>&1 >/dev/null && eval "$(portmaster-start --data=/opt/safing/portmaster completion zsh)"
