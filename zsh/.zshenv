unset HISTFILE

# Other XDG paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}

[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/user-dirs.dirs" ] && . "${XDG_CONFIG_HOME:-$HOME/.config}/user-dirs.dirs"

# Doesn't seem to work
export ANDROID_SDK_HOME="$XDG_CONFIG_HOME"/android
export ANDROID_AVD_HOME="$XDG_DATA_HOME"/android/
export ANDROID_EMULATOR_HOME="$XDG_DATA_HOME"/android/
export ADB_VENDOR_KEY="$XDG_CONFIG_HOME"/android

# Disable files
export LESSHISTFILE=-

# Fixing Paths
export MBSYNCRC="$XDG_CONFIG_HOME"/isync/mbsyncrc
export ATOM_HOME="$XDG_DATA_HOME"/atom
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export XSERVERRC="$XDG_CONFIG_HOME"/X11/xserverrc
export GEM_SPEC_CACHE="$XDG_DATA_HOME/ruby/specs"
export GEM_HOME="$XDG_DATA_HOME/ruby/gems"
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export GOPATH="$XDG_DATA_HOME"/go
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GRADLE_USER_HOME="$XDG_DATA_HOME"/gradle
export NUGET_PACKAGES="$XDG_CACHE_HOME"/NuGetPackages
export RXVT_SOCKET="$XDG_RUNTIME_DIR"/urxvtd
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export ZDOTDIR=$HOME/.config/zsh
#export HISTFILE="$XDG_DATA_HOME"/zsh/history
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export WEECHAT_HOME="$XDG_CONFIG_HOME"/weechat
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass
export NOTMUCH_CONFIG="$XDG_CONFIG_HOME"/notmuch/notmuchrc
export NMBGIT="$XDG_DATA_HOME"/notmuch/nmbug
export WINEROOT="$XDG_DATA_HOME"/wineprefixes
export WINEPREFIX="$WINEROOT"/default

export MPD_HOST="$XDG_CONFIG_HOME/mpd/socket"

# Scaling
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export QT_SCALE_FACTOR=1
export QT_SCREEN_SCALE_FACTORS="1;1;1"
export GDK_SCALE=1
export GDK_DPI_SCALE=1

# Theming
export QT_QPA_PLATFORMTHEME=qt5ct

# Configure apps
export BEMENU_OPTS="-H 36 --fn \"mononoki Nerd Font Mono 24\" --nb \"#000000\" --nf \"#c5c8c6\" --hb \"#39c1ed\" --hf \"#000000\" --cb \"#000000\" --cf \"#000000\" --tf \"#000000\" --tb \"#39c1ed\" --fb \"#000000\" --ff \"#c5c8c6\" --fbb \"#39c1ed\" --fbf \"#198844\""

# Fix wayland stuff
export MOZ_ENABLE_WAYLAND=1
export XKB_DEFAULT_OPTIONS=caps:escape
export XDG_CURRENT_DESKTOP=Unity

# Default Apps
export EDITOR="nvim"
export READER="zathura"
export VISUAL="nvim"
export TERMINAL="alacritty"
export BROWSER="browser"
export VIDEO="mpv"
export IMAGE="sxiv"
export OPENER="xdg-open"
export PAGER="less"

# DIRS
export SRCDIR="$HOME/.local/src"

# KSH
export ENV="$XDG_CONFIG_HOME/mksh/mkshrc"

# PATH
export PATH="$HOME/.local/bin/testing"
export PATH="$PATH:$HOME/.local/bin/shortcmds"
export PATH="$PATH:$HOME/.local/bin/generics"
export PATH="$PATH:$HOME/.local/bin/scripts"
export PATH="$PATH:$HOME/.local/bin/sp"
export PATH="$PATH:$HOME/.local/bin/blocks"
export PATH="$PATH:$HOME/.local/bin/ignore"
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:$HOME/.local/bin/flatpak-sc"
export PATH="$PATH:$HOME/.local/share/flatpak/exports/bin/"
export PATH="$PATH:$CARGO_HOME/bin"
export PATH="$PATH:$GOPATH/bin"
export PATH="$PATH:/opt/REAPER"
export PATH="$PATH:/usr/local/bin"
export PATH="$PATH:/usr/local/sbin"
export PATH="$PATH:/opt/safing/portmaster"
export PATH="$PATH:/var/lib/flatpak/exports/bin"
export PATH="$PATH:/usr/bin"
export PATH="$PATH:/usr/sbin"
export PATH="$PATH:/bin"
export PATH="$PATH:/sbin"

[ -f /etc/profile.d/nix-daemon.sh ] && . /etc/profile.d/nix-daemon.sh
