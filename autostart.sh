#!/bin/sh
runifnot () {
  if type $1 >/dev/null; then
    echo $1
    if [ -z "$(pgrep -Uzachir -f $1)" ]; then
      $@ &
    fi
  fi
}
killandrun () {
  if type $1 >/dev/null; then
    echo $1
    if [ -n "$(pgrep -Uzachir -f $1)" ]; then
      pkill -Uzachir $1
    fi
    $@ &
  fi
}

if [ -n "${WAYLAND_DISPLAY}${DISPLAY}" ]; then
  notify-sound.sh off &
  runifnot swayidle
  killandrun hyprpaper
  runifnot waybar
  gsettings set $gnome-schema gtk-theme 'Plata-Noir-Compact'
  gsettings set $gnome-schema icon-theme 'Mint-Y-Dark-Blue'
  gsettings set $gnome-schema cursor-theme 'Adwaita'
  xrdb ~/.Xresources
  runifnot portmaster-start --data=/opt/safing/portmaster notifier
  runifnot jamesdsp -t
fi

[ -x "$XDG_CONFIG_HOME/computerrc.sh" ] && "$XDG_CONFIG_HOME/computerrc.sh"
