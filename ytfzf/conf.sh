YTFZF_EXTMENU=' bemenu -i -l 30 -p "Search:" '

YTFZF_PLAYER='mpv --save-position-on-quit'
YTFZF_PLAYER_FORMAT='mpv --save-position-on-quit --ytdl-format='

cache_dir="$XDG_CACHE_HOME/ytfzf"
history_file="$cache_dir/ytfzf_hst"
current_file="$cache_dir/ytfzf_cur"
thumbnail_viewer="catimg"
