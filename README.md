## Zachir's Config Files

Hello! These are all the config files I thought could potentially be useful, both to myself to make reinstalling Linux faster, as well as for anyone interested. There are a few notes I feel I should make:

- I have a custom dwm build, which sources 'autostart.sh' and 'autostart_blocking.sh' from __*.config/dwm*__ not *.dwm*. This is available [here](https://gitlab.com/zachir/dwm-zir).
- I also have a custom st build, which sources '.Xresources' (this is a common enough patch, but I thought I would mention it, as I include my .Xresources file) available [here](https://gitlab.com/zachir/st-zir).
- I have gone through everything to remove personal information, however I might have missed something. If that happens, this repo will be updated, and the branch history deleted.

My personal installation method is as follows (performed immediately following a clean install):
`git clone https://gitlab.com/zachir/dotfiles ~/.config
cd ~/.config
make install`

Note that the `make install` will only install the files in `HOME` to their corresponding place in the home dir as hard links. Make sure to save changes to these files before cloning! Additionally, if you only want my configs for specific programs, I would recommend cloning it into a separate directory and avoiding the `make install`.
