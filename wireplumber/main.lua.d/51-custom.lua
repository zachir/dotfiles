table.insert(alsa_monitor.rules,
  {
    -- Rules for matching a device or node. It is an array of
    -- properties that all need to match the regexp. If any of the
    -- matches work, the actions are executed for the object.
    matches = {
      {
        { "device.name", "matches", "alsa_card.usb-Audient_Audient_iD4-00" },
      },
    },
    -- Apply properties on the matched object.
    apply_properties = {
      -- Use UCM instead of profile when available. Can be
      -- disabled to skip trying to use the UCM profile.
      ["api.alsa.use-ucm"] = false,

      -- The default active profile. Is by default set to "Off".
      ["device.profile"] = "Pro Audio",
    },
  }
)

